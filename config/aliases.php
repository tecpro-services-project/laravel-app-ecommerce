<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */
    'ProductMgr' => Tecpro\Ecommerce\Scripts\Managers\Facades\ProductMgr::class,
    'ProductCategoryMgr' => Tecpro\Ecommerce\Scripts\Managers\Facades\ProductCategoryMgr::class,
    'ProductSlotMgr' => Tecpro\Ecommerce\Scripts\Managers\Facades\ProductSlotMgr::class,
    'ProductSearch' => Tecpro\Ecommerce\Scripts\Managers\Facades\ProductSearch::class,
];
