<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Tecpro\Ecommerce\Database\Enums\ProductSlotType;

class CreateProductSlotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_slot', function (Blueprint $table) {
            $table->string('id', 32)->primary();
            $table->boolean('is_enable')->nullable()->default(false);
            $table->enum('type', [
                ProductSlotType::$MANUAL,
                ProductSlotType::$WITH_SORTING
            ])->default(ProductSlotType::$MANUAL);
            $table->string('description', 256)->nullable();
            $table->text('config')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_slot');
    }
}
