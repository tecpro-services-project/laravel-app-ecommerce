<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_detail', function (Blueprint $table) {
            $table->string('id', 32);
            $table->boolean('is_enable')->nullable()->default(false);
            $table->string('name', 128)->nullable();
            $table->string('title', 128)->nullable();
            $table->string('description', 1024)->nullable();
            $table->string('keyword', 1024)->nullable();
            $table->string('locale_id', 8);
            $table->primary(['id', 'locale_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_detail');
    }
}
