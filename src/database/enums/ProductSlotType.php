<?php

namespace Tecpro\Ecommerce\Database\Enums;

class ProductSlotType
{
    public static $MANUAL = '1';
    public static $WITH_SORTING = '2';
}
