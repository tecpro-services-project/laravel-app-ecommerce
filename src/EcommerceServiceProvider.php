<?php

namespace Tecpro\Ecommerce;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class EcommerceServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $ecommerceConfig = config('ecommerce');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutes();
        $this->loadViewsFrom(__DIR__ . '/resources/views', Arr::get($ecommerceConfig, 'namespace.view'));
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', Arr::get($ecommerceConfig, 'namespace.lang'));
        $this->loadSingletonFacade();
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/aliases.php',
            'app.aliases'
        );
        $this->mergeConfigFrom(
            __DIR__ . '/../config/ecommerce.php',
            'ecommerce'
        );
    }

    /**
     * Load all routes for administrator
     */
    protected function loadRoutes()
    {
        // Add prefix /admin for all URL
        $adminConfig = config('admin');
        Route::name(Arr::get($adminConfig, 'group.name'))
            ->prefix(Arr::get($adminConfig, 'group.prefix'))
            ->middleware('web')
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/routes/ecommerce.php');
            });
    }

    /**
     * Load App singleton facade
     */
    public function loadSingletonFacade()
    {
        $this->app->singleton('productMgr', function () {
            return new \Tecpro\Ecommerce\Scripts\Managers\ProductMgr();
        });

        $this->app->singleton('productCategoryMgr', function () {
            return new \Tecpro\Ecommerce\Scripts\Managers\ProductCategoryMgr();
        });

        $this->app->singleton('productSlotMgr', function () {
            return new \Tecpro\Ecommerce\Scripts\Managers\ProductSlotMgr();
        });

        $this->app->singleton('productSearch', function () {
            return new \Tecpro\Ecommerce\Scripts\SearchEngines\ProductSearch();
        });
    }
}
