<?php

namespace Tecpro\Ecommerce\Scripts\Managers;

use Tecpro\Core\Scripts\Managers\Abstracts\DefaultMgr;
use Tecpro\Ecommerce\App\Models\ProductSlot;
use Tecpro\Ecommerce\Database\Enums\ProductSlotType;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductMgr as ProductMgrFacade;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductSearch;

class ProductSlotMgr extends DefaultMgr
{
/**
     * Select the product slot based on the given content ID
     * @param string $recordId product slot ID
     * @return \Tecpro\CMS\App\Models\ProductSlot|null Return product slot model or null
     */
    public function get(string $recordId)
    {
        return ProductSlot::where('id', '=', $recordId)->get()->first();
    }

    /**
     * Select the product slot based on the given IDs array
     * @param array $recordIds product slot ID array
     * @return \Illuminate\Database\Eloquent\Collection The product slot model collecion
     */
    public function getMultiple(array $recordIds)
    {
        return ProductSlot::whereIn('id', $recordIds)->get();
    }

    /**
     * Create new product slot
     * @param array $record The product slot data record
     * @return \Tecpro\CMS\App\Models\ProductSlot The created product slot model
     */
    public function create(array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return (new ProductSlot)->filterCreate($record);
    }

    /**
     * Update product slot based on the given ID
     * @param string $recordId The product slot ID
     * @param array $record The product slot data record
     * @return \Tecpro\CMS\App\Models\ProductSlot The product slot model which is updated
     */
    public function update(string $recordId, array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        return (new ProductSlot)->where('id', '=', $recordId)->update($record);
    }

    /**
     * Delete product slot based on the given ID
     * @param string $recordId The product slot ID
     */
    public function delete(string $recordId)
    {
        ProductSlot::where('id', '=', $recordId)->delete();
    }

    /**
     * Get and render content in the slot configuration
     * @param string $slotId The content slot ID
     * @return \Illuminate\Database\Eloquent\Collection|null Return product collection or null
     */
    public function getAndRender(string $slotId)
    {
        $productSlot = $this->get($slotId);

        if (!isset($productSlot)) return null;

        $productSlotAssoc = $productSlot->transform();
        $productCollection = null;

        switch ($productSlotAssoc['type']) {
            case ProductSlotType::$MANUAL:
                $slotItems = preg_split("/\r\n|\n|\r/", $productSlotAssoc['config']);
                $productCollection = ProductMgrFacade::getMultiple($slotItems);
                break;

            case ProductSlotType::$WITH_SORTING:
                $productCollection = ProductSearch::setSorting('oldest')->search();
                break;
            
            default:
                
                break;
        }

        return $productCollection;
    }
}
