<?php

namespace Tecpro\Ecommerce\Scripts\Managers;

use Tecpro\Core\Scripts\Managers\Abstracts\DefaultMgr;
use Tecpro\Ecommerce\App\Models\Product;
use Tecpro\Core\Scripts\Support\Extension\ExtensionFactory;

class ProductMgr extends DefaultMgr
{
    /**
     * @var \Illuminate\Database\Eloquent\Model $productModel The manager model
     */
    protected $productModel;

    public function __construct()
    {
        $this->productModel = ExtensionFactory::get('models.product');

        if (!isset($this->productModel)) {
            $this->productModel = Product::class;
        }
    }

    /**
     * Select the product based on the given content ID
     * @param string $recordId product ID
     * @return \Tecpro\Ecommerce\App\Models\Product The product model
     */
    public function get(string $recordId)
    {
        $product = $this->productModel::where('id', '=', $recordId)->get()->first();
        return $product;
    }

    /**
     * Select the product based on the given IDs array
     * @param array $recordIds product ID array
     * @return \Illuminate\Database\Eloquent\Collection The product model collecion
     */
    public function getMultiple(array $recordIds)
    {
        // Implode order from array to id1,id2,id3,...
        $recordIdOrdered = trim(implode(',', $recordIds));
        // Replace order to 'id1','id2','id3',... for querying string
        $recordIdOrdered = preg_replace_callback('/([^,]*)/', function ($matches) {
            return "'$matches[0]'";
        }, $recordIdOrdered);
        return $this->productModel::whereIn('id', $recordIds)->orderByRaw("FIELD(id, $recordIdOrdered)")->get();
    }

    /**
     * Create new product
     * @param array $record The product data record
     * @return \Tecpro\Ecommerce\App\Models\Product The created product model
     */
    public function create(array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        $newProduct = $this->getProductModel();
        $newProduct->filterCreate($record)->newDetail()->filterCreate($record);
        return $newProduct;
    }

    /**
     * Update product based on the given ID
     * @param string $recordId The product ID
     * @param array $record The product data record
     * @return \Tecpro\Ecommerce\App\Models\Product The product model which is updated
     */
    public function update(string $recordId, array $record)
    {
        $record['is_enable'] = $this->isEnable($record);
        $productModel = $this->get($recordId);
        $productModel->filterUpdate($record)->getDetailModel($record['locale_id'])->filterUpdate($record);
        return $productModel;
    }

    /**
     * Delete product based on the given ID
     * @param string $recordId The product ID
     */
    public function delete(string $recordId)
    {
        $this->productModel::where('id', '=', $recordId)->delete();
    }

    /**
     * Get sub category of parent category
     * @param ?string $parentId The category parent ID
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator The content category model collecion
     */
    public function getAll(?int $pagingAmount = null)
    {
        return isset($pagingAmount) && $pagingAmount > 0 ? $this->productModel::paginate($pagingAmount)->withQueryString() : $this->productModel::all();
    }

    /**
     * Select the product based on the given content ID
     * @return \Tecpro\Ecommerce\App\Models\Product The product model
     */
    public function getProductModel() {
        return new $this->productModel();
    }
}
