<?php

namespace Tecpro\Ecommerce\Scripts\Managers\Facades;

use Illuminate\Support\Facades\Facade;

class ProductMgr extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'productMgr';
    }
}
