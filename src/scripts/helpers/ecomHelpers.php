<?php

use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductSlotMgr;

if (!function_exists('productSlot')) {
    /**
     * Render the product slot
     * @param string $id The product slot ID
     * @param string $localeId The locale ID
     * @return string return Rendered HTML string
     */
    function productSlot(string $id, string $localeId)
    {
        $productSlot = ProductSlotMgr::get($id);

        echo "<!-- Slot ID: $id -->";
        if (!isset($productSlot)) {
            return __('ecom::product.slot.error.not.found.with.id', ['id' => $id]);
        }

        $productSlotAssoc = $productSlot->transform();

        // TODO: Need to continue to develop this function
    }
}
