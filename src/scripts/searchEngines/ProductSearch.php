<?php

namespace Tecpro\Ecommerce\Scripts\SearchEngines;

use Tecpro\Core\Scripts\Managers\Abstracts\Search;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductMgr;

class ProductSearch extends Search
{
    /**
     * Use filter rule and sorting rule to search for the products
     * @return \ProductSearch The product model collecion
     */
    public function search()
    {
        $productModel = ProductMgr::getProductModel();
        $sortRule = $this->getSortingRuleById($this->getSorting());
        $this->searchResultCollection = $productModel->orderBy(...$sortRule)->get();

        return $this;
    }
}
