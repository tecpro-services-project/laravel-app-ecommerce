<?php

namespace Tecpro\Ecommerce\Scripts\Objects;

use Error;
use Tecpro\Core\Scripts\Objects\CoreObject;

class Price extends CoreObject
{
    /**
     * @var float The price float value
     */
    protected $priceValue;

    /**
     * @var string The price format
     */
    protected $priceFormat;

    /**
     * Initialize the Product Price Model
     */
    public function __construct($price, string $localeId)
    {
        if (!is_numeric($price)) {
            throw new Error('Money variable should be a numeric. ' . gettype($price) . ' is given.');
        }

        $dPrice = is_int($price) ? $price : floatval($price);
        $this->priceValue = $dPrice;
        $this->priceFormat = number_format($this->priceValue) . __('global.' . $localeId . '.currency');
    }

    /**
     * @return integer The price value
     */
    public function getPriceValue()
    {
        return $this->priceValue;
    }

    /**
     * @return string The price format
     */
    public function getPriceFormat()
    {
        return $this->priceFormat;
    }
}
