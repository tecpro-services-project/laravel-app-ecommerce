<?php

use Illuminate\Support\Facades\Route;
use Tecpro\Ecommerce\App\Http\Controllers\ProductController;
use Tecpro\Ecommerce\App\Http\Controllers\ProductCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
| All request will have the prefix /admin/...
|
*/

/**
 * Only authenticated account can access these route
 */
Route::name('ecom.')
    ->prefix('ecom')
    ->middleware('auth:admin')
    ->group(
        function () {
            /**
             * Host prefix will be /admin/ecom/product/xxx
             */
            Route::prefix('product')->group(function () {
                Route::get('/list', [ProductController::class, 'showProductList'])->name('product.list');
                Route::get('/form', [ProductController::class, 'showProductForm'])->name('product.form');
                Route::post('/create', [ProductController::class, 'createProduct'])->name('product.create');
                Route::post('/update', [ProductController::class, 'updateProduct'])->name('product.update');
                Route::post('/delete', [ProductController::class, 'deleteProduct'])->name('product.delete');
            });

            /**
             * Host prefix will be /admin/ecom/category/xxx
             */
            Route::prefix('category')->group(function () {
                Route::get('/list', [ProductCategoryController::class, 'showProductCategoryList'])->name('category.list');
                Route::get('/form', [ProductCategoryController::class, 'showProductCategoryForm'])->name('category.form');
                Route::post('/create', [ProductCategoryController::class, 'createProductCategory'])->name('category.create');
                Route::post('/update', [ProductCategoryController::class, 'updateProductCategory'])->name('category.update');
                Route::post('/delete', [ProductCategoryController::class, 'deleteProductCategory'])->name('category.delete');
            });
        }
    );
