<?php

namespace Tecpro\Ecommerce\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class ProductCategory extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_category';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'parent_id',
        'path',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Return product detail relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Product detail relation hasMany
     */
    public function detail()
    {
        return $this->hasMany(ProductCategoryDetail::class, 'id', 'id');
    }

    /**
     * Get new product detail model
     * @return \Tecpro\Ecommerce\App\Models\ProductCategoryDetail The product detail model 
     */
    public function newDetail()
    {
        return new ProductCategoryDetail();
    }

    /**
     * Get the product detail model
     * @param string $localeId The locale ID
     * @return \Tecpro\Ecommerce\App\Models\ProductCategoryDetail|null The product detail model 
     */
    public function getDetailModel(string $localeId)
    {
        return $this->detail()->where('locale_id', $localeId)->first();
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        $final = $this->toArray();
        $endPoint = 'product/category/' . $this->id;
        $items = [];

        foreach ($this->items()->get() as $item) {
            array_push($items, $item->transform($localeId));
        }

        return array_merge($final, [
            'detail' => $this->getDetailModel($localeId)->transform(),
            'isAllowUpdateKey' => $this->isAllowUpdateKey,
            'endPoint' => $endPoint,
            'url' => url($endPoint),
            'items' => $items
        ]);
    }

    /**
     * Return category product items relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Category product items relation hasMany
     */
    public function items()
    {
        return $this->hasMany(ProductCategoryItem::class, 'category_id', 'id');
    }

    /**
     * Get new category product item
     * @return \Tecpro\Ecommerce\App\Models\ProductCategoryItem The category product item 
     */
    public function newItem()
    {
        return new ProductCategoryItem();
    }
}
