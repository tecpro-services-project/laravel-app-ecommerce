<?php

namespace Tecpro\Ecommerce\App\Models;

use Tecpro\Core\App\Models\CoreModel;
use Tecpro\Ecommerce\Scripts\Objects\Price;

class Product extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'price',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     */
    public $incrementing = false;

    /**
     * Return product detail relation hasMany
     * @return \Illuminate\Database\Eloquent\Relations\HasMany Product detail relation hasMany
     */
    public function detail() {
        return $this->hasMany(ProductDetail::class, 'id', 'id');
    }

    /**
     * Get new product detail model
     * @return \Tecpro\Ecommerce\App\Models\ProductDetail The product detail model 
     */
    public function newDetail() {
        return new ProductDetail();
    }

    /**
     * Get the product detail model
     * @param string $localeId The locale ID
     * @return \Tecpro\Ecommerce\App\Models\ProductDetail|null The product detail model 
     */
    public function getDetailModel(string $localeId) {
        return $this->detail()->where('locale_id', $localeId)->first();
    }

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '') {
        $final = $this->toArray();

        return array_merge($final, [
            'detail' => $this->getDetailModel($localeId)->toArray(),
            'price' => (new Price($final['price'], $localeId))->toArray(),
            'imageUrl' => isset($final['image']) ? asset($final['image']) : '',
        ]);
    }
}
