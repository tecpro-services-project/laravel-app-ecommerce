<?php

namespace Tecpro\Ecommerce\App\Models;

use Tecpro\Core\App\Models\CoreModel;

class ProductSlot extends CoreModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_slot';

    /**
     * The "type" of the ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'is_enable',
        'type',
        'description',
        'config',
        'created_at',
        'updated_at'
    ];

    /**
     * Tell Laravel the primary key is not increment integer
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * Transform all necessary data into an associative array
     * @param string $localeId The locale ID
     * @return array
     */
    public function transform(string $localeId = '')
    {
        return $this->toArray();
    }
}
