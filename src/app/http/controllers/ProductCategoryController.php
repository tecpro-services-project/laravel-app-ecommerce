<?php

namespace Tecpro\Ecommerce\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductCategoryMgr;

class ProductCategoryController extends Controller
{
    /**
     * Show the product category list
     * @param \Illuminate\Http\Request The HTTP request
     * @return \Illuminate\Contracts\View\View Render product category list view
     */
    public function showProductCategoryList(Request $request)
    {
        $parentId = $request->input('parentId');
        $productCategoryPaging = ProductCategoryMgr::getSubCategoryOf($parentId, 10);

        if (!isset($parentId)) {
            return view('ecom::category.categoryList', [
                'productCategoryPaging' => $productCategoryPaging,
                'parentId' => $parentId,
                'prevParentId' => ''
            ]);
        } else {
            return view(
                'ecom::category.categoryListWithSub',
                [
                    'catergory' => [
                        'id' => 'Technology-Frontend-category',
                        'name' => 'Technology-Frontend-category',
                        'is_enabled' => true,
                        'template' => 'template_a'
                    ],
                    'breadcrumbs' => [
                        'Home' => '/',
                        'SiteID' => '/siteid',
                        'Category' => '/catergory',
                        'Technology-Frontend-category' => '/cat',
                    ],
                    'templates' => [
                        'template-a',
                        'template-b',
                        'template-default',
                    ],
                    'subCategories' => [
                        [
                            'id' => 'example-enabled-category',
                            'name' => 'Example enabled category',
                            'description' => 'Example category which is enabled',
                            'is_enabled' => true
                        ],
                        [
                            'id' => 'example-disabled-category',
                            'name' => 'Example disabled category',
                            'description' => 'Example category which is disabled',
                            'is_enabled' => false
                        ]
                    ],
                    'products' => [
                        [
                            'id' => 'example-enabled-product',
                            'name' => 'Example enabled product',
                            'description' => 'Example product which is enabled',
                            'is_enabled' => true
                        ],
                        [
                            'id' => 'example-disabled-product',
                            'name' => 'Example disabled product',
                            'description' => 'Example product which is disabled',
                            'is_enabled' => false
                        ]
                    ]
                ]
            );
        }
    }
}
