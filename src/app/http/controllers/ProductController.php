<?php

namespace Tecpro\Ecommerce\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PDOException;
use Tecpro\Core\Scripts\Support\Form\FormTable;
use Tecpro\Ecommerce\App\Http\Requests\ProductFormRequest;
use Tecpro\Ecommerce\Scripts\Managers\Facades\ProductMgr;

class ProductController extends Controller
{
    /**
     * Show the product list
     * @return \Illuminate\Contracts\View\View The product list view
     */
    public function showProductList()
    {
        $productPaging = ProductMgr::getAll(10);

        return view('ecom::product.productList', [
            'productPaging' => $productPaging
        ]);
    }

    /**
     * Show the product form
     * @return \Illuminate\Contracts\View\View The product list form and its data (optional)
     */
    public function showProductForm(Request $request)
    {
        $id = $request->input('id');
        $localeId = $request->input('localeId') ?? 'vi';
        $product = null;
        $productForm = new FormTable();
        $productForm->setTables(['product', 'product_detail']);
        $productForm->setMethod('POST');
        $productForm->setAction(route('admin.ecom.product.create'));
        $productForm->setConfig(config('admin.forms.default'));
        $productForm->finalize();

        if (isset($id)) {
            $product = ProductMgr::get($id);

            if (!isset($product)) {
                abort(404, 'Product not found');
            }

            $productForm->setAction(route('admin.ecom.product.update'));
            $productForm->setFormData($product->toArray());
            $productForm->setFormData($product->getDetailModel($localeId)->transform($localeId));
        }

        return view('ecom::product.productForm', [
            'product' => $product,
            'productForm' => $productForm
        ]);
    }

    /**
     * Create the product
     * @param \Tecpro\Ecommerce\App\Http\Requests\ProductFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function createProduct(ProductFormRequest $request)
    {
        $validated = $request->validated();
        $errorMessage = '';
        $newProduct = null;
        $validated['id'] = Str::slug($validated['id']);

        try {
            DB::beginTransaction();
            $newProduct = ProductMgr::create($validated);
            DB::commit();
        } catch (Error | PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.ecom.product.form', [
                'id' => $newProduct->id
            ]);
        }
    }

    /**
     * Update the product
     * @param \Tecpro\Ecommerce\App\Http\Requests\ProductFormRequest The HTTP form request
     * @return \Illuminate\Http\RedirectResponse Redirect to the form
     */
    public function updateProduct(ProductFormRequest $request)
    {
        $validated = $request->validated();
        $prevId = $request->input('prevId');
        $errorMessage = '';
        $product = ProductMgr::get($prevId);
        $productId = Str::slug($validated['id']);

        if (!isset($product)) {
            return redirect()->back()->withErrors([
                'errorMessage' => __('ecom::content.asset.error.not.found')
            ]);
        }

        try {
            unset($validated['id']);
            DB::beginTransaction();
            ProductMgr::update($prevId, $validated);
            DB::commit();
        } catch (Error | PDOException $error) {
            DB::rollBack();
            $errorMessage = $error->getMessage();
        }

        if (strlen($errorMessage) !== 0) {
            return redirect()->back()->withErrors([
                'errorMessage' => $errorMessage
            ]);
        } else {
            return redirect()->route('admin.ecom.product.form', [
                'id' => $productId
            ]);
        }
    }
}
