<?php

namespace Tecpro\Ecommerce\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * Product
             */
            'id' => 'required|max:32',
            'price' => 'required',
            'student_amount' => '',
            'start_register_date' => '',
            'end_register_date' => '',
            'start_learn_date' => '',
            'end_learn_date' => '',
            'instructor_id' => '',            

            /**
             * Product detail
             */
            'is_enable' => '',
            'name' => 'max:128',
            'title' => 'max:128',
            'description' => 'max:1024',
            'keyword' => 'max:1024',
            'course_duration' => '',
            'course_description' => 'max:65535',
            'course_program' => 'max:65535',
            'course_faq' => 'max:65535',
            'course_announcement' => 'max:65535',
            'locale_id' => 'required|max:8',
        ];
    }
}
