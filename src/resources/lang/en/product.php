<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product text
    |--------------------------------------------------------------------------
    */
    'product.heading.list.label' => 'Product list',
    'product.create.button' => 'New product',
    'product.list.select.all.label' => 'Select',
    'product.name.form.label' => 'Product name',
    'product.list.action.label' => 'Action',
    'heading.form.label' => 'Product form',
    'product.id.form.label' => 'Product ID',
    'product.price.form.label' => 'Product price',
    'product.student_amount.form.label' => 'Student amount',
    'product.start_register_date.form.label' => 'Start register date',
    'product.end_register_date.form.label' => 'End register date',
    'product.start_learn_date.form.label' => 'Start learn date',
    'product.end_learn_date.form.label' => 'End learn date',
    'product.image.form.label' => 'Image',
    'product.instructor_id.form.label' => 'Instructor',
    'product_detail.is_enable.form.label' => 'Online',
    'product_detail.name.form.label' => 'Product Name',
    'product_detail.title.form.label' => 'Title',
    'product_detail.description.form.label' => 'Description',
    'product_detail.keyword.form.label' => 'Keyword',
    'product_detail.course_duration.form.label' => 'Course duration',
    'product_detail.course_description.form.label' => 'Course description',
    'product_detail.course_program.form.label' => 'Course program',
    'product_detail.course_faq.form.label' => 'Course FAQ',
    'product_detail.course_announcement.form.label' => 'Course announcement',
    'update.form.label' => 'Update',
    'submit.form.label' => 'Submit',
    'slot.error.not.found.with.id' => 'Product slot not found'
];
