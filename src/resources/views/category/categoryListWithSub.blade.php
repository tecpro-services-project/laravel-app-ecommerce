@extends('admin::layouts.default')
@section('title', 'Category')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="category-header mb-5">
                    <div class="category-header__breadcrump my-2">
                        @foreach ($breadcrumbs as $key => $path)
                            @if ($key !== $catergory['name'])
                                <a href="{{ $path }}">{{ $key }}</a> /
                            @else
                                <a href="{{ $path }}">{{ $key }}</a>
                            @endif
                        @endforeach
                    </div>
                    <h1 class="category-header__title mb-0">
                        {{ $catergory['name'] }}
                    </h1>
                    <p class="category-header__subtitle lead text-muted mb-2">
                        ID: <span class="value">{{ $catergory['id'] }}</span>
                    </p>
                    <div class="category-header__status status">
                        @if ($catergory['is_enabled'])
                            (icon) Active
                        @else
                            Inactive
                        @endif
                        <span> - </span>
                        <span> Last modified at 15:00 AM 17 Dec 2022 </span>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="category-template mb-5">
                    <h1 class="category-template__title title mb-0">
                        Template
                    </h1>
                    <hr class="p-0 m-0 my-2">
                    <div class="category-template__content ml-3">
                        <form class="form mb-2">
                            <label for="template" class="form-label">
                                Category UI template:
                            </label>
                            <select name="template" id="template-select" class="form-select mb-2">
                                <option value="">Choose a template</option>
                                @foreach ($templates as $template)
                                    <option value="{{ $template }}">{{ $template }}</option>
                                @endforeach
                            </select>
                            <button type="submit" class="btn btn-info text-white px-4">Apply</button>
                        </form>
                        <div class="status">
                            (icon) This template {{ $catergory['template'] }} is enabled at 10:00 AM 17 Dec, 2022
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="category-subcategories mb-5">
                    <div class="d-flex">
                        <h1 class="category-subcategories__title flex-fill title mb-0">
                            SubCategories
                        </h1>
                        <button class="btn btn-primary text-white">Assign Category</button>
                    </div>
                    <hr class="p-0 m-0 my-2">
                    <div class="category-subcategories__content ml-3">
                        <div class="table-responsive mb-3">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Category Id</th>
                                        <th>Category Name</th>
                                        <th>Description</th>
                                        <th>Enabled</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subCategories as $index => $categoryRow)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $categoryRow['id'] }}</td>
                                            <td>{{ $categoryRow['name'] }}</td>
                                            <td>{{ $categoryRow['description'] }}</td>
                                            <td>{{ $categoryRow['is_enabled'] }}</td>
                                            <td>{{ $index + 1 }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="paging-and-action d-flex">
                            <div class="paging flex-fill"></div>
                            <div class="paging">
                                <button class="btn btn-info text-white px-4">
                                    Apply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="category-products mb-5">
                    <div class="d-flex">
                        <h1 class="category-products__title flex-fill title mb-0">
                            Product
                        </h1>
                        <button class="btn btn-primary text-white">Assign Category</button>
                    </div>
                    <hr class="p-0 m-0 my-2">
                    <div class="category-products__content ml-3">
                        <div class="table-responsive mb-3">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Product Id</th>
                                        <th>Product Name</th>
                                        <th>Description</th>
                                        <th>Enabled</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subCategories as $index => $categoryRow)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $categoryRow['id'] }}</td>
                                            <td>{{ $categoryRow['name'] }}</td>
                                            <td>{{ $categoryRow['description'] }}</td>
                                            <td>{{ $categoryRow['is_enabled'] }}</td>
                                            <td>{{ $index + 1 }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="paging-and-action d-flex">
                            <div class="paging flex-fill"></div>
                            <div class="paging">
                                <button class="btn btn-info text-white px-4">
                                    Apply
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
