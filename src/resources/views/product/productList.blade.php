@extends('admin::layouts.default')
@section('title', 'Product list')
@section('content')

<div class="page">
    <div class="page-wrapper">
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <!-- Page pre-title -->
                        <h1 class="page-title">
                            {{ __('ecom::product.product.heading.list.label') }}
                        </h1>
                    </div>
                    <!-- Page title actions -->
                    <div class="col-auto ms-auto d-print-none">
                        <div class="btn-list">
                            <span class="d-none d-sm-inline">
                                <a href="{{ route('admin.ecom.product.form') }}" class="btn">
                                    {{ __('ecom::product.product.create.button') }}
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pt-4 product page__item-list">
            <div class="table-responsive product__list item-list__table">
                <table class="table table-vcenter">
                    <thead>
                        <tr>
                            <th style="width: 10%" class="text-center"><a href="#">{{ __('ecom::product.product.list.select.all.label') }}</a></th>
                            <th>{{ __('ecom::product.product.id.form.label') }}</th>
                            <th>{{ __('ecom::product.product.name.form.label') }}</th>
                            <th>{{ __('ecom::product.product.list.action.label') }}</th>
                        </tr>
                    </thead>
                    <tbody>
    
                        @foreach ($productPaging->items() as $product)
                        @php ($productAssoc = $product->transform('vi'))

                        <tr>
                            <td class="text-center">
                                <input class="form-check-input" type="checkbox">
                            </td>
                            <td>
                                <a href="{{ route('admin.ecom.product.form', ['id' => $productAssoc['id']]) }}">{{ $productAssoc['id'] }}</a>
                            </td>
                            <td>
                                <a href="{{ route('admin.ecom.product.form', ['id' => $productAssoc['id']]) }}">{{ $productAssoc['detail']['name'] }}</a>
                            </td>
                            <td>
                                <a href="{{ route('admin.ecom.product.form', ['id' => $productAssoc['id']]) }}">Edit</a>
                                <a href="{{ route('admin.ecom.product.delete', ['id' => $productAssoc['id']]) }}">Delete</a>
                            </td>
                        </tr>

                        @endforeach
                        
                    </tbody>
                </table>
            </div>
    
            <div class="product__paging item-list__paging">
    
                {{-- {{ $productPaging->render() }} --}}
    
                <div class="row">
                    <div class="col-3">
                        <a href="" class="button">Previous</a>
                    </div>
                    <div class="col-3">
                        <a href="" class="button">Next</a>
                    </div>
                </div>

            </div>
    
        </div>
    </div>
</div>

@endsection
